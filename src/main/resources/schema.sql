create table person_tb(

                          id integer not null,
                          username varchar(255) not null,
                          gender varchar (10) not null,
                          bio varchar(255) not null,
                          primary key (id)
);

insert into person_tb (id,username,gender,bio) values(1,'data1','male','bio');
insert into person_tb (id,username,gender,bio) values(2,'data2','male','bio');
insert into person_tb (id,username,gender,bio) values(3,'data3','male','bio');