package com.kshrd.srspringh2console.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Person {
    private  int id;
    private  String username;
    private  String gender;
    private  String bio;
}
