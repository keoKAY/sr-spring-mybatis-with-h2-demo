package com.kshrd.srspringh2console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrSpringH2ConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrSpringH2ConsoleApplication.class, args);
    }

}
