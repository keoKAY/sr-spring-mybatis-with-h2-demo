package com.kshrd.srspringh2console.controller;


import com.kshrd.srspringh2console.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller

public class HomeController {

    @Autowired
    PersonRepository personRepository;


    @GetMapping
    public String index(){
        System.out.println("Here is the value of the person: ");
        personRepository.getAllStudent().stream().forEach(System.out::println);
        return "index";
    }
}
