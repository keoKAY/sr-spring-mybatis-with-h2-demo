package com.kshrd.srspringh2console.repository;


import com.kshrd.srspringh2console.model.Person;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository {


    @Select("select * from person_tb")
    List<Person> getAllStudent();
}
